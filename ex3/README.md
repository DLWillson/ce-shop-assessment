# Exercise 3 - The Pipeline Doc

Create a diagram(s) with supporting design document that defines a CICD process on AWS. The CICD process should result in a deployment to the autoscaling AWS technology of choice. The design should start when a developer pushes code and end when the application is deployed and running on the infrastructure. The VCS system can be GitHub or CodeCommit. The diagram, or series of diagrams, should have enough granular detail that an engineer could begin implementation.

## TLDR

1. Go [here](https://gitlab.com/DLWillson/ce-shop-assessment/-/pipelines)
2. Click the topmost green "Passed" button.
3. Click on jobs to see what they (ought to) do and/or read [the CI config file](/.gitlab-ci.yml), [the build script](build), and [the deploy script](deploy).

## Details

This skeleton builds from the words "autoscaling AWS technology". Of course, All this can also be done in K8s with HPA (horizontal pod autoscaler).

This tries to adhere to these patterns:
- one AMI per product, per version
- AMI is clean: no environment-specific settings and no secrets

It assumes:
- VPCs, Application Load-Balancers, and Security Groups are in-place and correct enough.
- AutoScaling Groups and Launch Profiles have been configured and are being updated, rather than replaced.
- DOMAIN_NAME is enough key to figure everything else out, *and* I can get it to a new machine so it can download its config.

This willfully ignores deployment strategies: downtime, canary, blue-green, soak, etc...

[Return](/README.md)
