from random import randint

def create_random_array():
    table=[]
    while len(table) < 4:
        row=[]
        while len(row) < 4:
            row.append(randint(-4,5))
        row.sort()
        table.append(row)
        # DEBUG print(row)
        # DEBUG print(table)
    return table
