# Exercise 1 - The Function

Write a function that takes in a 2 dimensional array and returns the number of negative numbers. Note: the numbers always increase or stay the same as the array index increases.

![Example Sorted 2d Array](example_sorted_2d_array.png)

## TLDR

```python
def count_negatives (sorted_2d_array):
    negatives=0
    for row in sorted_2d_array:
        for cell in row:
            if cell < 0:
                negatives += 1
            else:
                # This optimezation is possible because "the numbers always increase or stay the same as the array index increases"
                break
    return negatives
```

## Prove it

```bash
cd ce-shop-assessment/ex1/
./count-negs
```

Assumes bash and Python3

[Return](/README.md)
