# Exercise 2 - The SQL Query

Construct an SQL query to find a reservation for an individual whose last name is something Stephenson or Stevenson or Stevensen.

The reservation table has Reservation, CustomerID and Date

The customer table has CustomerID, FirstName and LastName

## TLDR

```sql
select * from customer natural join reservation where lastname in ('Stephenson', 'Stevenson', 'Stevensen');
--or
select * from customer natural join reservation where lastname like 'Ste%ns_n';
```

## Prove it

```bash
cd ce-shop-assessment/ex2/
./ceshop-steves
```

Assumes a Linux machine with Docker and nothing else running in Docker / a little luck.

With Linux, Docker, and no luck, the IP address in `ceshop-steves` may need to be updated.

[Return](/README.md)
