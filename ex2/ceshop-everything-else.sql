create table customer (
    customer_id serial primary key,
    firstname varchar (256) not null,
    lastname varchar (1024) not null
);

create table reservation (
    reservation_id serial primary key,
    customer_id int not null,
    res_date timestamp not null,
    constraint fk_customer
    foreign key(customer_id)
      references customer(customer_id)
);

insert into customer(firstname,lastname) values
    ('David', 'Willson'),
    ('David', 'Aranda'),
    ('David', 'Pentico'),
    ('Alice', 'Stephenson'),
    ('Bob',   'Stevenson'),
    ('Chuck', 'Stevensen'),
    ('Eve',   'Stefanson'),
    ('Adolph Blaine Charles David Earl Frederick Gerald Hubert Irvin John Kenneth Lloyd Martin Nero Oliver Paul Quincy Randolph Sherman Thomas Uncas Victor William Xerxes Yancy Zeus','Wolfeschlegel­steinhausen­bergerdorff­welche­vor­altern­waren­gewissenhaft­schafers­wessen­schafe­waren­wohl­gepflege­und­sorgfaltigkeit­beschutzen­vor­angreifen­durch­ihr­raubgierig­feinde­welche­vor­altern­zwolfhundert­tausend­jahres­voran­die­erscheinen­von­der­erste­erdemensch­der­raumschiff­genacht­mit­tungstein­und­sieben­iridium­elektrisch­motors­gebrauch­licht­als­sein­ursprung­von­kraft­gestart­sein­lange­fahrt­hinzwischen­sternartig­raum­auf­der­suchen­nachbarschaft­der­stern­welche­gehabt­bewohnbar­planeten­kreise­drehen­sich­und­wohin­der­neue­rasse­von­verstandig­menschlichkeit­konnte­fortpflanzen­und­sich­erfreuen­an­lebenslanglich­freude­und­ruhe­mit­nicht­ein­furcht­vor­angreifen­vor­anderer­intelligent­geschopfs­von­hinzwischen­sternartig­raum');

insert into reservation(customer_id,res_date) values
    (1, '1999-12-31'),
    (2, '2000-01-01'),
    (3, '2000-01-02'),
    (4, '2000-01-03'),
    (5, '2000-01-04'),
    (6, '2000-01-05'),
    (7, '2000-01-06');

-- create view many

create view many_stevensons as select * from customer natural join reservation where lastname in ('Stephenson', 'Stevenson', 'Stevensen');

-- create view like

create view like_stevensons as select * from customer natural join reservation where lastname like 'Ste%ns_n';
